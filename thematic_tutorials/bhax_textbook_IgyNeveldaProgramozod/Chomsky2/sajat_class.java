import java.util.ArrayList;

class sajat_class {
    public static void main(String[] args)
    {        
        myclass mc = new myclass();

        mc.add(10);
        mc.add(3);
        mc.add(2);
        mc.add(8);

        System.out.println(mc.contains(2));
        System.out.println(mc.contains(0));
        System.out.println(mc.sort());
    }

    static class myclass
    {
        private elem root = new elem();
        private ArrayList<Integer> l = new ArrayList<Integer>();
        
        private class elem
        {
            int szam;
            elem jobb;
            elem bal;
            boolean empty;
            elem()
            {
                empty=true;
            }
        }
        private boolean search(elem e, int i)
        {
            if (e==null||e.empty)
                return false;
            if ((int)e.szam==(int)i)
                return true;
            if ((int)e.szam>(int)i)
                return search(e.bal,i);
            else
                return search(e.jobb,i);
        }
        private void recursive_add(elem e, int i)
        {
            if (e.empty)
            {
                e.empty=false;
                e.szam=i;
            }
            if ((int)e.szam>(int)i)
            {
                if (e.bal==null)
                e.bal=new elem();
                recursive_add(e.bal,i);
            }
            if ((int)e.szam<(int)i)
            {
                if (e.jobb==null)
                e.jobb=new elem();
                recursive_add(e.jobb,i);
            }
        }

        void add(int i)
        {
            l.add(i);
            recursive_add(root,i);
        }
        boolean contains(int i)
        {
            if (root==null)
            {
                System.out.println("root is null");
            }
            return search(root,i);
            
        }
        ArrayList<Integer> sort()
        {
            for (int i=0;i<l.size()-1;i++)
                for (int j=0;j<l.size()-i-1;j++)
                    if((int)l.get(j)>(int)l.get(j+1))
                    {
                        int tmp=l.get(j);
                        l.set(j, l.get(j+1));
                        l.set(j+1, tmp);
                    }
            return l;
        }
    }
}