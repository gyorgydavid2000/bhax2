/*
 * Labirintus.java
 *
 * DIGIT 2005, Javat tanitok
 * Batfai Norbert, nbatfai@inf.unideb.hu
 *
 */
package javattanitok.labirintus;
/**
 * A labirintust leiro osztaly.
 *
 * @author Batfai Norbert, nbatfai@inf.unideb.hu
 * @version 0.0.1
 */
public class Labirintus {
    /** A labirintus szelessege. */
    protected int szelesseg;
    /** A labirintus magassaga. */
    protected int magassag;
    /** A labirintus szerkezete: hol van fal, hol jarat. */
    protected boolean[][] szerkezet;
    /** A falat a true ertek jelenti. */
    final static boolean FAL = true;
    /** Milyen allapotban van eppen a jatek. */
    protected int jatekallapot = 0;
    /** Normal mukodes, a hossel idokozben semmi nem tortent. */
    public static final int JaTeK_MEGY_HoS_RENDBEN = 0;    
    /** A host eppen megettek, de meg van elete. */
    public final static int JaTeK_MEGY_MEGHALT_HoS = 1;
    /** Vege a jateknak, a jatekos gyozott. */
    public final static int JaTeK_VeGE_MINDEN_KINCS_MEGVAN = 2;
    /** Vege a jateknak, a jatekos vesztett. */
    public final static int JaTeK_VeGE_MEGHALT_HoS = 3;
    /** A labirintus kincsei. */
    protected Kincs [] kincsek;
    /** A labirintus szornyei. */
    protected Szorny [] szornyek;
    /**
     * Letrehoz egy megadott szerkezetu
     * {@code Labirintus} objektumot.
     */
    public Labirintus() {
        szerkezet = new boolean[][]{
            
    {false, false,  false, true,  false, true,  false, true,  true,  true },
    {false, false, false, false, false, false, false, false, false, false},
    {true,  false, true,  false, true,  false, true,  false, true,  false},
    {false, false, false, false, true,  false, true,  false, false, false},
    {false, true,  true,  false, false, false, true,  true,  false, true },
    {false, false, false, false, true,  false, false, false, false, false},
    {false,  true,  false, false,  false, true,  false, true,  true,  false},
    {false,  false, false, true,  false, true,  false, true,  false, false},
    {false, true, false, false, false, false, false, false, false, true },
    {false, false, false, false,  true,  false, false, false,  true,  true }
    
        };
        
        magassag = szerkezet.length;
        szelesseg = szerkezet[0].length;
        
    }
    /**
     * Letrehoz egy parameterben kapott szerkezetu <code>Labirintus</code> 
     * objektumot.
     *
     * @param      kincsekSzama       a kincsek szama a labirintusban.
     * @param      szornyekSzama      a szornyek szama a labirintusban.
     * @exception  RosszLabirintusKivetel  ha a labirintust definialo tomb 
     * nincs elkeszitve.
     */
    public Labirintus(boolean[][] szerkezet, int kincsekSzama, int szornyekSzama)
    throws RosszLabirintusKivetel {
        
        if(szerkezet == null)
            throw new RosszLabirintusKivetel("A labirintust definialo tomb nincs elkeszitve.");
        
        this.szerkezet = szerkezet;
        
        magassag = szerkezet.length;
        szelesseg = szerkezet[0].length;
        
        kincsekSzornyek(kincsekSzama, szornyekSzama);
        
    }
    /**
     * Letrehoz egy megadott meretu, veletlen szerkezetu
     * <code>Labirintus</code> objektumot.
     *
     * @param      szelesseg          a labirintus szelessege.
     * @param      magassag           a labirintus magassaga.
     * @param      kincsekSzama       a kincsek szama a labirintusban.
     * @param      szornyekSzama      a szornyek szama a labirintusban.
     */
    public Labirintus(int szelesseg, int magassag,
            int kincsekSzama, int szornyekSzama) {
        
        this.magassag = magassag;
        this.szelesseg = szelesseg;
        
        szerkezet = new boolean[magassag][szelesseg];
        java.util.Random veletlenGenerator = new java.util.Random();
        
        for(int i=0; i<magassag; ++i)
            for(int j=0; j<szelesseg; ++j)
                if(veletlenGenerator.nextInt()%3 == 0)
                    // a labirintus egy harmada lesz fal
                    szerkezet[magassag][szelesseg] = false;
                else
                    // ket harmada pedig jarat
                    szerkezet[magassag][szelesseg] = true;
        
        kincsekSzornyek(kincsekSzama, szornyekSzama);
        
    }
    /**
     * Letrehoz egy 10x10-es, beepitett szerkezetu <code>Labirintus</code>
     * objektumot.
     *
     * @param      kincsekSzama       a kincsek szama a labirintusban.
     * @param      szornyekSzama      a szornyek szama a labirintusban.
     */
    public Labirintus(int kincsekSzama, int szornyekSzama) {
        
        this();
        
        magassag = szerkezet.length;
        szelesseg = szerkezet[0].length;
        
        kincsekSzornyek(kincsekSzama, szornyekSzama);
        
    }
    /**
     * Egy megfelelo szerkezetu szoveges allomanybol elkeszit egy uj a 
     * <code>Labirintus</code> objektumot.
     * A szoveges allomany szerkezete a kovetkezo:
     * <pre>
     * // A labirintus szerkezetet megado allomany, szerkezete a kovetkezo:
     * // a kincsek szama
     * // a szornyek szama
     * // a labirintus szelessege
     * // magassaga
     * // fal=1 jarat=0 ...
     * // .
     * // .
     * // .
     * 6
     * 3
     * 10
     * 10
     * 0 0 0 1 0 1 0 1 1 1
     * 0 0 0 0 0 0 0 0 0 0
     * 1 0 1 0 1 0 1 0 1 0
     * 0 0 0 0 1 0 1 0 0 0
     * 0 1 1 0 0 0 1 1 0 1
     * 0 0 0 0 1 0 0 0 0 0
     * 0 1 0 0 0 1 0 1 1 0
     * 0 0 0 1 0 1 0 1 0 0
     * 0 1 0 0 0 0 0 0 0 1
     * 0 0 0 0 1 0 0 0 1 1
     * </pre>
     *
     * @param      labirintusFajlNev       a labirintust definialo, megfelelo 
     * szerkezetu szoveges allomany neve.
     * @exception  RosszLabirintusKivetel  ha a labirintust definialo allomany 
     * nincs meg, nem a megfelelo szerkezetu, vagy gond van az olvasasaval.
     */
    public Labirintus(String labirintusFajlNev) throws RosszLabirintusKivetel {
        
        int kincsekSzama = 6;  // ezeknek a kezdoertekeknek nincs jelentosege,
        int szornyekSzama = 3; // mert majd a fajlbol olvassuk be, amiben ha a 
        // negy fo adat hibas, akkor nem is epitjuk fel a labirintust.
        
        // Csatorna a szoveges allomany olvasasahoz
        java.io.BufferedReader szovegesCsatorna = null;
        
        try {
            szovegesCsatorna = new java.io.BufferedReader(
                    new java.io.FileReader(labirintusFajlNev));
            
            String sor = szovegesCsatorna.readLine();
            
            while(sor.startsWith("//"))
                sor = szovegesCsatorna.readLine();
            
            try {
                
                kincsekSzama = Integer.parseInt(sor);
                
                sor = szovegesCsatorna.readLine();
                szornyekSzama = Integer.parseInt(sor);
                
                sor = szovegesCsatorna.readLine();
                szelesseg = Integer.parseInt(sor);
                
                sor = szovegesCsatorna.readLine();
                magassag = Integer.parseInt(sor);
                
                szerkezet = new boolean[magassag][szelesseg];
                
            } catch(java.lang.NumberFormatException e) {
                
                throw new RosszLabirintusKivetel("Hibas a kincsek, szornyek szama, szelesseg, magassag megadasi resz.");
                
            }
            
            for(int i=0; i<magassag; ++i) {
                
                sor = szovegesCsatorna.readLine();
                
                java.util.StringTokenizer st =
                        new java.util.StringTokenizer(sor);
                
                for(int j=0; j<szelesseg; ++j) {
                    String tegla = st.nextToken();
                    
                    try {
                        
                        if(Integer.parseInt(tegla) == 0)
                            szerkezet[i][j] = false;
                        else
                            szerkezet[i][j] = true;
                        
                    } catch(java.lang.NumberFormatException e) {
                        
                        System.out.println(i+". sor "+j+". oszlop "+e);
                        szerkezet[i][j] = false;
                        
                    }
                }
            }
            
        } catch(java.io.FileNotFoundException e1) {
            
            throw new RosszLabirintusKivetel("Nincs meg a fajl: " + e1);
            
        } catch(java.io.IOException e2) {
            
            throw new RosszLabirintusKivetel("IO kivetel tortent: "+e2);
            
        } catch(java.util.NoSuchElementException e3) {
            
            throw new RosszLabirintusKivetel("Nem jo a labirintus szerkezete: "
                    +e3);
            
        } finally {
            
            if(szovegesCsatorna != null) {
                
                try{
                    szovegesCsatorna.close();
                } catch(Exception e) {}
                
            }
            
        }
        
        // Ha ide eljutottunk, akkor felepult a labirintus,
        // lehet benepesiteni:
        kincsekSzornyek(kincsekSzama, szornyekSzama);
        
    }
    /**
     * Letrehozza a kincseket es a szornyeket.
     *
     * @param      kincsekSzama       a kincsek szama a labirintusban.
     * @param      szornyekSzama      a szornyek szama a labirintusban.
     */
    private void kincsekSzornyek(int kincsekSzama, int szornyekSzama) {
        // Kincsek letrehozasa
        kincsek = new Kincs[kincsekSzama];
        for(int i=0; i<kincsek.length; ++i)
            kincsek[i] = new Kincs(this, (i+1)*100);
        // Szornyek letrehozasa
        szornyek = new Szorny[szornyekSzama];
        for(int i=0; i<szornyek.length; ++i)
            szornyek[i] = new Szorny(this);
        
    }
    /**
     * Megadja a jatek aktualis allapotat.
     *
     * @return int a jatek aktualis allapota.
     */
    public int allapot() {
        
        return jatekallapot;
        
    }
    /**
     * A labirintus mikrovilag eletenek egy pillanata: megnezi, hogy a bolyongo
     * hos ratalalt-e a kincsekre, vagy a szornyek a hosre. Ennek megfeleloen
     * megvaltozik a jatek allapota.
     *
     * @param hos aki a labirintusban bolyong.
     * @return int a jatek allapotat leiro kod.
     */
    public int bolyong(Hos hos) {
        
        boolean mindMegvan = true;
        
        for(int i=0; i < kincsek.length; ++i) {
            
            // A hos ratalalt valamelyik kincsre?
            if(kincsek[i].megtalalt(hos))
                hos.megtalaltam(kincsek[i]);
            
            // ha ez egyszer is teljesul, akkor nincs minden kincs megtalalva
            if(!kincsek[i].megtalalva())
                mindMegvan = false;
            
        }
        
        if(mindMegvan) {
            
            jatekallapot = JaTeK_VeGE_MINDEN_KINCS_MEGVAN;
            return jatekallapot;
            
        }
        
        for(int i=0; i < szornyek.length; ++i) {
            
            szornyek[i].lep(hos);
            
            if(szornyek[i].megesz(hos))  {
                jatekallapot = JaTeK_MEGY_MEGHALT_HoS;
                
                if(hos.megettek())
                    jatekallapot = JaTeK_VeGE_MEGHALT_HoS;
                
                return jatekallapot;
            }
            
        }
        
        return JaTeK_MEGY_HoS_RENDBEN;
    }
    /**
     * Madadja, hogy fal-e a labirintus adott oszlop, sor pozicioja.
     *
     * @param oszlop a labirintus adott oszlopa
     * @param sor a labirintus adott sora
     * @return true ha a pozicio fal vagy nincs a labirintusban.
     */
    public boolean fal(int oszlop, int sor) {
        
        if(!(oszlop >= 0 && oszlop <= szelesseg-1
                && sor >= 0 && sor <= magassag-1))
            return FAL;
        else
            return szerkezet[sor][oszlop] == FAL;
        
    }
    /**
     * Madadja a labirintus szelesseget.
     *
     * @return int a labirintus szelessege.
     */
    public int szelesseg() {
        
        return szelesseg;
        
    }
    /**
     * Madadja a labirintus magassagat.
     *
     * @return int a labirintus magassaga.
     */
    public int magassag() {
        
        return magassag;
        
    }
    /**
     * Megadja a labirintus szerkezetet.
     *
     * @return boolean[][] a labirintus szerkezete.
     */
    public boolean[][] szerkezet() {
        
        return szerkezet;
        
    }
    /**
     * Megadja a labirintus kincseit.
     *
     * @return Kincs[] a labirintus kincsei.
     */
    public Kincs[] kincsek() {
        
        return kincsek;
        
    }
    /**
     * Megadja a labirintus szornyeit.
     *
     * @return Szorny[] a labirintus szornyei.
     */
    public Szorny[] szornyek() {
        
        return szornyek;
        
    }
    /**
     * Kinyomtatja a labirintus szerkezetet a System.out-ra.
     */
    public void nyomtat() {
        
        for(int i=0; i<magassag; ++i) {
            for(int j=0; j<szelesseg; ++j) {
                
                if(szerkezet[i][j])
                    System.out.print("|FAL");
                else
                    System.out.print("|   ");
                
            }
            
            System.out.println();
            
        }
        
    }
    /**
     * Kinyomtatja a labirintus szerkezetet es szereploit a System.out-ra.
     *
     * @param hos akit szinten belenyomtat a labirintusba.
     */
    public void nyomtat(Hos hos) {
        
        for(int i=0; i<magassag; ++i) {
            for(int j=0; j<szelesseg; ++j) {
                
                boolean vanSzorny = vanSzorny(i, j);
                boolean vanKincs = vanKincs(i, j);
                boolean vanHos = (i == hos.sor() && j == hos.oszlop());
                
                if(szerkezet[i][j])
                    System.out.print("|FAL");
                else if(vanSzorny && vanKincs && vanHos)
                    System.out.print("|SKH");
                else if(vanSzorny && vanKincs)
                    System.out.print("|SK ");
                else if(vanKincs && vanHos)
                    System.out.print("|KH ");
                else if(vanSzorny && vanHos)
                    System.out.print("|SH ");
                else if(vanKincs)
                    System.out.print("|K  ");
                else if(vanHos)
                    System.out.print("|H  ");
                else if(vanSzorny)
                    System.out.print("|S  ");
                else
                    System.out.print("|   ");
                
            }
            
            System.out.println();
            
        }
        
    }
    /**
     * Kinyomtatja a labirintus szerkezetet es szereploit egy
     * karakteres csatornaba.
     *
     * @param hos akit szinten belenyomtat a labirintusba.
     * @param csatorna ahova nyomtatunk.
     */
    public void nyomtat(Hos hos, java.io.PrintWriter csatorna) {
        
        for(int i=0; i<magassag; ++i) {
            for(int j=0; j<szelesseg; ++j) {
                
                boolean vanSzorny = vanSzorny(i, j);
                boolean vanKincs = vanKincs(i, j);
                boolean vanHos = (i == hos.sor() && j == hos.oszlop());
                
                if(szerkezet[i][j])
                    csatorna.print("|FAL");
                else if(vanSzorny && vanKincs && vanHos)
                    csatorna.print("|SKH");
                else if(vanSzorny && vanKincs)
                    csatorna.print("|SK ");
                else if(vanKincs && vanHos)
                    csatorna.print("|KH ");
                else if(vanSzorny && vanHos)
                    csatorna.print("|SH ");
                else if(vanKincs)
                    csatorna.print("|K  ");
                else if(vanHos)
                    csatorna.print("|H  ");
                else if(vanSzorny)
                    csatorna.print("|S  ");
                else
                    csatorna.print("|   ");
                
            }
            
            csatorna.println();
            
        }
        
    }
    /**
     * Kinyomtatja a labirintus szerkezetet es szereploit egy sztringbe.
     *
     * @param hos akit szinten belenyomtat a labirintusba.
     * @return String a kinyomtatott labirintus
     */
    public String kinyomtat(Hos hos) {
        
        StringBuffer stringBuffer = new StringBuffer();
        
        for(int i=0; i<magassag; ++i) {
            for(int j=0; j<szelesseg; ++j) {
                
                boolean vanSzorny = vanSzorny(i, j);
                boolean vanKincs = vanKincs(i, j);
                boolean vanHos = (i == hos.sor() && j == hos.oszlop());
                
                if(szerkezet[i][j])
                    stringBuffer.append("|FAL");
                else if(vanSzorny && vanKincs && vanHos)
                    stringBuffer.append("|SKH");
                else if(vanSzorny && vanKincs)
                    stringBuffer.append("|SK ");
                else if(vanKincs && vanHos)
                    stringBuffer.append("|KH ");
                else if(vanSzorny && vanHos)
                    stringBuffer.append("|SH ");
                else if(vanKincs)
                    stringBuffer.append("|K  ");
                else if(vanHos)
                    stringBuffer.append("|H  ");
                else if(vanSzorny)
                    stringBuffer.append("|S  ");
                else
                    stringBuffer.append("|   ");
                
            }
            
            stringBuffer.append("\n");
            
        }
        
        return stringBuffer.toString();
    }
    /**
     * Madadja, hogy van-e megtalalhato kincs a labirintus
     * adott oszlop, sor pozicioja.
     *
     * @param oszlop a labirintus adott oszlopa
     * @param sor a labirintus adott sora
     * @return true ha van.
     */
    boolean vanKincs(int sor, int oszlop) {
        
        boolean van = false;
        
        for(int i=0; i<kincsek.length; ++i)
            if(sor == kincsek[i].sor()
            && oszlop == kincsek[i].oszlop()
            && !kincsek[i].megtalalva()) {
            van = true;
            break;
            }
        
        return van;
    }
    /**
     * Madadja, hogy van-e szorny a labirintus adott oszlop,
     * sor pozicioja.
     *
     * @param oszlop a labirintus adott oszlopa
     * @param sor a labirintus adott sora
     * @return true ha van.
     */
    boolean vanSzorny(int sor, int oszlop) {
        
        boolean van = false;
        
        for(int i=0; i<szornyek.length; ++i)
            if(sor == szornyek[i].sor()
            && oszlop == szornyek[i].oszlop()) {
            van = true;
            break;
            }
        
        return van;
    }
    /**
     * A labirintussal kapcsolatos aprosagok onallo kiprobalasara
     * szolgal ez az indito metodus.
     *
     * @param args parancssor-argumentumok nincsenek.
     */
    public static void main(String[] args) {
        
        Labirintus labirintus = new Labirintus(6, 3);
        Hos hos = new Hos(labirintus);
        
        System.out.println(labirintus.getClass());
        System.out.println(hos.getClass());
        
    }
}
