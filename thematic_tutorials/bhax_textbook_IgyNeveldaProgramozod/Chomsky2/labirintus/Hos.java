/*
 * Hos.java
 *
 * DIGIT 2005, Javat tanitok
 * Batfai Norbert, nbatfai@inf.unideb.hu
 *
 */
package javattanitok.labirintus;
/**
 * A labirintus hoset leiro osztaly.
 *
 * @author Batfai Norbert, nbatfai@inf.unideb.hu
 * @version 0.0.1
 * @see javattanitok.labirintus.Labirintus
 */
public class Hos extends Szereplo {
    /** A labirintusban megtalalt kincsek ertekei. */
    protected int megtalaltertekek;
    /** A hos eleteinek maximalis szama. */
    public static final int eLETEK_SZaMA = 5;
    /** A hos eleteinek szama. */
    protected int eletekSzama = eLETEK_SZaMA;
    /**
     * Letrehoz egy <code>Hos</code> objektumot.
     *
     * @param      labirintus       amelyben a hos bolyongani fog.
     */
    public Hos(Labirintus labirintus) {
        super(labirintus);
        megtalaltertekek = 0;
    }
    /**
     * Gyujtogeti a megtalalt kincseket.
     *
     * @param      kincs       amit eppen magtalalt a hos.
     */
    public void megtalaltam(Kincs kincs) {
        
        megtalaltertekek += kincs.ertek();
        
    }
    /**
     * Jelzi, hogy eppen megettek.
     *
     * @return true ha a hosnek meg van elete, ellenkezo esetben, 
     * azaz ha az osszes elete elfogyott mar, akkor false.
     */
    public boolean megettek() {
        
        if(eletekSzama > 0) {
            --eletekSzama;
            return false;
        } else
            return true;
        
    }
    /**
     * megmondja, hogy elek-e meg?
     *
     * @return true ha a hosnek meg van elete, ellenkezo esetben, azaz 
     * ha az osszes elete elfogyott mar, akkor false.
     */
    public boolean el() {
        
        return eletekSzama > 0;
        
    }
    /**
     * Megadja az eletek szamat.
     *
     * @return int az eletek szama.
     */
    public int eletek() {
        
        return eletekSzama;
        
    }
    /**
     * Megadja a megtalalt kincsek osszegyujtogetett ertekeit.
     *
     * @return int a megtalalt kincsek osszegyujtogetett ertekei.
     */
    public int pontszam() {
        
        return megtalaltertekek;
        
    }
   /**
     * A labirintus, amiben a hos mozog.
     *
     * @return Labirintus a labirintus.
     */
    public Labirintus labirintus() {
        
        return labirintus;
        
    }      
}
