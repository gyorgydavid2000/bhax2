/*
 * Szereplo.java
 *
 * DIGIT 2005, Javat tanitok
 * Batfai Norbert, nbatfai@inf.unideb.hu
 *
 */
package javattanitok.labirintus;
/**
 * A labirintus szereploit (kincsek, szornyek, hos) absztrahalo osztaly.
 *
 * @author Batfai Norbert, nbatfai@inf.unideb.hu
 * @version 0.0.1 
 * @see javattanitok.labirintus.Labirintus
 */
public class Szereplo {
    /** A szereplo oszlop pozicioja. */
    protected int oszlop;
    /** A szereplo sor pozicioja. */
    protected int sor;
    /** A labirintus, melyben a szereplo van. */
    protected Labirintus labirintus;
    /** A labirintus szelessege. */
    protected int maxSzelesseg;
    /** A labirintus magassaga. */
    protected int maxMagassag;
    /** Veletlenszam generator a szereplok mozgatasahoz. */
    protected static java.util.Random veletlenGenerator 
            = new java.util.Random();
    /**
     * Letrehoz egy <code>Szereplo</code> objektumot.
     *
     * @param      labirintus       amibe a szereplot helyezzuk.
     */
    public Szereplo(Labirintus labirintus) {
        this.labirintus = labirintus;
        maxSzelesseg = labirintus.szelesseg();
        maxMagassag = labirintus.magassag();
        // A szereplo kezdo pozicioja a labirintusban
        szereploHelyeKezdetben();
    }
    /**
     * A szereplo labirintusbeli kezdo poziciojanak meghatarozasa.
     */
    void szereploHelyeKezdetben() {
        // Tobbszor probalkozunk elhelyezni a szereplot a labirintusban,
        // szamolja, hol tartunk ezekkel a probalkozasokkal:
        int szamlalo = 0;
        
        do {
            // itt +2,-2-k, hogy a bal also saroktol tavol tartsuk
            // a szereploket, mert majd ezt akarjuk a hos kezdo poziciojanak
            oszlop = 2+veletlenGenerator.nextInt(maxSzelesseg-2);
            sor = veletlenGenerator.nextInt(maxMagassag-2);
            // max. 10-szer probalkozunk, de ha sikerul nem "falba tenni" a
            // szereplot, akkor maris kilepunk:
        } while(++szamlalo<10 && labirintus.fal(oszlop, sor));
        
    }
    /**
     * A szereplo felfele lep. Ha nem tud, helyben marad.
     */
    public void lepFol() {
        
        if(sor > 0)
            if(!labirintus.fal(oszlop, sor-1))
                --sor;        
    }
    /**
     * A szereplo lefele lep. Ha nem tud, helyben marad.
     */
    public void lepLe() {
        
        if(sor < maxMagassag-1)
            if(!labirintus.fal(oszlop, sor+1))
                ++sor;
        
    }
    /**
     * A szereplo balra lep. Ha nem tud, helyben marad.
     */
    public void lepBalra() {
        
        if(oszlop > 0)
            if(!labirintus.fal(oszlop-1, sor))
                --oszlop;
        
    }
    /**
     * A szereplo jobbra lep. Ha nem tud, helyben marad.
     */
    public void lepJobbra() {
        
        if(oszlop < maxSzelesseg-1)
            if(!labirintus.fal(oszlop+1, sor))
                ++oszlop;
        
    }
    /**
     * A szereplo (Euklideszi) tavolsaga egy masik szereplotol a labirintusban.
     *
     * @param szereplo a masik szereplo
     * @return int tavolsag a masik szereplotol.
     */
    public int tavolsag(Szereplo szereplo) {
        
        return (int)Math.sqrt((double)
        (oszlop - szereplo.oszlop)*(oszlop - szereplo.oszlop)
        + (sor - szereplo.sor)*(sor - szereplo.sor)
        );
        
    }
    /**
     * Egy pozicio (Euklideszi) tavolsaga egy masik szereplotol a 
     * labirintusban.
     *
     * @param oszlop a pozicio oszlop koordinataja
     * @param sor a pozicio sor koordinataja
     * @param szereplo a masik szereplo
     * @return int tavolsag a masik szereplotol.
     */
    public int tavolsag(int oszlop, int sor, Szereplo szereplo) {
        
        if(!(oszlop >= 0 && oszlop <= maxSzelesseg-1
                && sor >= 0 && sor <= maxMagassag-1))
            return Integer.MAX_VALUE;
        else
            return (int)Math.sqrt((double)
            (oszlop - szereplo.oszlop)*(oszlop - szereplo.oszlop)
            + (sor - szereplo.sor)*(sor - szereplo.sor)
            );
        
    }
    /**
     * Beallitja a szereplo labirintusbeli poziciojanak oszlop 
     * koordinatajat.
     *
     * @param oszlop a szereplo labirintusbeli poziciojanak oszlop 
     * koordinataja.
     */
    public void oszlop(int oszlop) {
        
        this.oszlop = oszlop;
        
    }
    /**
     * Beallitja a szereplo labirintusbeli poziciojanak sor koordinatajat.
     *
     * @param sor a szereplo labirintusbeli poziciojanak sor koordinataja.
     */
    public void sor(int sor) {
        
        this.sor = sor;
        
    }
    /**
     * Megadja a szereplo labirintusbeli poziciojanak oszlop koordinatajat.
     *
     * @return int a szereplo labirintusbeli poziciojanak oszlop koordinataja.
     */
    public int oszlop() {
        
        return oszlop;
        
    }
    /**
     * Megadja a szereplo labirintusbeli poziciojanak sor koordinatajat.
     *
     * @return int a szereplo labirintusbeli poziciojanak sor koordinataja.
     */
    public int sor() {
        
        return sor;
        
    }

    public String toString() {
    
        return "SZEREPLo oszlop = " 
                + oszlop
                + " sor = "
                + sor;
    }    
}
