/*
 * Kincs.java
 *
 * DIGIT 2005, Javat tanitok
 * Batfai Norbert, nbatfai@inf.unideb.hu
 *
 */
package javattanitok.labirintus;
/**
 * A labirintus kincseit jellemzo osztaly.
 *
 * @author Batfai Norbert, nbatfai@inf.unideb.hu
 * @version 0.0.1
 * @see javattanitok.labirintus.Labirintus
 */
public class Kincs extends Szereplo {
    /** A kincs erteke. */
    protected int ertek;
    /** Megtalaltak mar? */
    protected boolean megtalalva;
    /**
     * Letrehoz egy {@code Kincs} objektumot.
     *
     * @param   labirintus  amibe a kincset helyezzuk.
     * @param   ertek       a kincs erteke.
     */
    public Kincs(Labirintus labirintus, int ertek) {
        super(labirintus);
        this.ertek = ertek;
    }
    /**
     * A szereplo (pl. hos, szornyek) megtalalta a kincset?
     *
     * @param   hos aki keresi a kincset.
     * @return true ha a kincset eppen most megtalalta a szereplo, 
     * ha eppen nem, vagy mar eleve megvan talalva a kincs, akkor false.
     */
    public boolean megtalalt(Szereplo szereplo) {
        
        if(megtalalva)
        // mert egy kicset csak egyszer lehet megtalalni
            return false;
        
        if(oszlop == szereplo.oszlop()
        && sor == szereplo.sor())
            megtalalva = true;
        
        return megtalalva;
    }
    /**
     * Megadja a kincs erteket.
     *
     * @return  int a kincs erteke.
     */
    public int ertek() {
        
        return ertek;
        
    }
    /**
     * Megmondja, hogy megtalaltak-e mar a kincset?
     *
     * @return true ha a kincset mar megtalaltak, 
     * ha meg nem akkor false.
     */
    public boolean megtalalva() {
        
        return megtalalva;
        
    }
    /**
     * A {@code Kincs} objektum sztring reprezentaciojat adja
     * meg.
     *
     * @return String az objektum sztring reprezentacioja.
     */
    public String toString() {
    
        return "KINCS ertek = " 
                + ertek
                + " megtalalva = "
                + megtalalva;
    }    
}
