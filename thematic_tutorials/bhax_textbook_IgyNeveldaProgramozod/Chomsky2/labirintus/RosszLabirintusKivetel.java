/*
 * RosszLabirintusKivetel.java
 *
 * DIGIT 2005, Javat tanitok
 * Batfai Norbert, nbatfai@inf.unideb.hu
 *
 */
package javattanitok.labirintus;
/**
 * Ha allomany alapjan keszitjuk a labirintust, akkor az allomany szerkezetenek
 * hibait jelzi ez az osztaly.
 *
 * @author Batfai Norbert, nbatfai@inf.unideb.hu
 * @version 0.0.1
 * @see javattanitok.labirintus.Labirintus
 */
public class RosszLabirintusKivetel extends java.lang.Exception {    
    /**
     * Elkeszit egy <code>RosszLabirintusKivetel</code> kivetel objektumot.
     *
     * @param hiba a hiba leirasa
     */
    public RosszLabirintusKivetel(String hiba) {
        super(hiba);
    }
}
