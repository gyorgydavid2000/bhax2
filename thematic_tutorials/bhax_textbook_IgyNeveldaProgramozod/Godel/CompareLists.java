import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class CompareLists {
    public static void main(String[] args)
    {
        final int n = 100000;
        Random r = new Random();
        List<Integer> al = new ArrayList<Integer>();
        List<Integer> ll = new LinkedList<Integer>();
        List<Integer> al2 = new ArrayList<Integer>();
        List<Integer> ll2 = new LinkedList<Integer>();

        //add
        long start = System.currentTimeMillis();
        for (int i=0;i<n;i++)
            al.add(i);
        long finish = System.currentTimeMillis();
        long time = finish - start;
        System.out.println("ArrayList add: " + time + " ms");

        start = System.currentTimeMillis();
        for (int i=0;i<n;i++)
            ll.add(i);
        finish = System.currentTimeMillis();
        time = finish - start;
        System.out.println("LinkedList add: " + time + " ms");

        for (int i=0;i<n;i++)
            al2.add(i);
        for (int i=0;i<n;i++)
            ll2.add(i);


        //get
        start = System.currentTimeMillis();
        for (int i=0;i<n;i++)
            al.get(r.nextInt(n));
        finish = System.currentTimeMillis();
        time = finish - start;
        System.out.println("ArrayList get: " + time + " ms");

        start = System.currentTimeMillis();
        for (int i=0;i<n;i++)
            ll.get(r.nextInt(n));
        finish = System.currentTimeMillis();
        time = finish - start;
        System.out.println("LinkedList get: " + time + " ms");


        //remove by index
        start = System.currentTimeMillis();
        for (int i=0;i<n;i++)
            al.remove(r.nextInt(n-i));
        finish = System.currentTimeMillis();
        time = finish - start;
        System.out.println("ArrayList remove: " + time + " ms");

        start = System.currentTimeMillis();
        for (int i=0;i<n;i++)
            ll.remove(r.nextInt(n-i));
        finish = System.currentTimeMillis();
        time = finish - start;
        System.out.println("LinkedList remove: " + time + " ms");


        //remove by value
        start = System.currentTimeMillis();
        for (int i=0;i<n;i++)
            al2.remove((Integer)r.nextInt(n));
        finish = System.currentTimeMillis();
        time = finish - start;
        System.out.println("ArrayList remove: " + time + " ms");

        start = System.currentTimeMillis();
        for (int i=0;i<n;i++)
            ll2.remove((Integer)r.nextInt(n));
        finish = System.currentTimeMillis();
        time = finish - start;
        System.out.println("LinkedList remove: " + time + " ms");
    }
}
