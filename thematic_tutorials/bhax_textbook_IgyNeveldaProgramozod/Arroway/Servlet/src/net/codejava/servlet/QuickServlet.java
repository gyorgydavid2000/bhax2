package net.codejava.servlet;
 
import java.io.IOException;
import java.io.PrintWriter;
 
import javax.servlet.ServletConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
public class QuickServlet extends HttpServlet {
 
    /**
     * this life-cycle method is invoked when this servlet is first accessed
     * by the client
     */
    public void init(ServletConfig config) {
        System.out.println("Servlet is being initialized");
    }
 
    /**
     * handles HTTP GET request
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
 
        PrintWriter writer = response.getWriter();
        writer.println("<html>Hello, I am a Java servlet!</html>");
        writer.flush();
    }
 
    /**
     * handles HTTP POST request
     */

	public static class elem
    {
        int szam;
        elem jobb;
        elem bal;
        public elem()
        {
            szam = 7;
            jobb = null;
            bal = null;
        }
    }

	static void Kiir(elem elemp, String ag, HttpServletResponse response)
    throws IOException {
	if (elemp.szam != -1)
		ag += elemp.szam;
	if (elemp.bal != null)
		Kiir(elemp.bal, ag,response);
	if (elemp.jobb != null)
		Kiir(elemp.jobb, ag,response);
	if (elemp.bal == null && elemp.jobb == null)
		{
		PrintWriter writer = response.getWriter();
		writer.println(ag+"<br/>");
		}
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException {

		elem root = new elem();
		root.szam = -1;
		elem hely = root;
		String be = request.getParameter("input");
		for (int i = 0; i < be.length(); i++)
		{
			if (be.charAt(i) == '0')
			{
				if (hely.bal == null)
				{
					elem uj = new elem();
					uj.szam = 0;
					hely.bal = uj;
					hely = root;
				}
				else
				{
					hely = hely.bal;
				}
			}
			if (be.charAt(i) == '1')
			{
				if (hely.jobb == null)
				{
					elem uj = new elem();
					uj.szam = 1;
					hely.jobb = uj;
					hely = root;
				}
				else
				{
					hely = hely.jobb;
				}
			}
		}

		PrintWriter writer = response.getWriter();
		writer.println("<html>");
		Kiir(root,"",response);
		writer.println("</html>");
        	writer.flush();
    }
 
    /**
     * this life-cycle method is invoked when the application or the server
     * is shutting down
     */
    public void destroy() {
        System.out.println("Servlet is being destroyed");
    }
}
