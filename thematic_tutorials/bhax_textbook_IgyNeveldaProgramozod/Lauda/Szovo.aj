public aspect Szovo {
        
    public pointcut fgvhivas(): call(public void HelloVilag.hello());

    before(): fgvhivas() {
        System.out.println("Ez a szöveget szúrjuk be a hello függvény elé");
    }

    after(): fgvhivas() {
        System.out.println("Ez a szöveget szúrjuk be a hello függvény mögé");
    }
}