import java.lang.Exception;

public class exceptions {
    public static void main(String[] args) throws Exception
    {
        //test(null);
        test("string");
        //test(1f);
    }

    public static class ParentException extends Exception {
    }
    public static class ChildException extends ParentException {
    }

    public static void test(Object input) throws Exception
    {
        try {
            System.out.println("Try!");
            if (input instanceof Float) {
                throw new ChildException();
            } else if (input instanceof String) {
                throw new ParentException();
            } else {
                throw new RuntimeException();
            }
        } catch (ChildException e) {
            System.out.println("Child Exception is caught!");
            if (e instanceof ParentException) {
                throw new ParentException();
            }
        } catch (ParentException e) {
            System.out.println("Parent Exception is caught!");
            System.exit(1);
        } catch (Exception e) {
            System.out.println("Exception is caught!");
        } finally {
            System.out.println("Finally!");
        }
    }
}