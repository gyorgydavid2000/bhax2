public aspect AOP
{
public pointcut callFun(char a) : call(public void LZWBinaryTree.insert(char)) && args(a);
    before(char a) : callFun(a)
    {
        System.out.println("Hozzádtuk a fához a "+ a +" element.");
    }
}
