import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FutureChainingExercise {
    private static ExecutorService executorService = Executors.newFixedThreadPool(2);

    public static void main(String[] args) {
        CompletableFuture<String> longTask = CompletableFuture.supplyAsync(() -> {
            sleep(1000);
            return "Hello";
        }, executorService);
        CompletableFuture<String> shortTask = CompletableFuture.supplyAsync(() -> {
            sleep(500);
            return "Hi";
        }, executorService);
        CompletableFuture<String> mediumTask = CompletableFuture.supplyAsync(() -> {
            sleep(750);
            return "Hey";
        }, executorService);
        //mivel csak 2 száll áll rendelkezésre, a mediumTask várakozik.

        CompletableFuture<String> result = longTask.applyToEitherAsync(shortTask, String::toUpperCase, executorService);
        //akkor van készen ha valamelyik kész (de várkozik a sorban),
        //azt adja vissza amelyik hamarabb végzett (de mindkettő végzett mire sorra kerül ezért a longTask),
        result = result.thenApply(s -> s + " World");

        CompletableFuture<Void> extraLongTask = CompletableFuture.supplyAsync(() -> {
            sleep(1500);
            return null;
        }, executorService);
        
        //System.out.println("test " + result.get());
        result = result.thenCombineAsync(mediumTask, (s1, s2) -> s2 + ", " + s1, executorService);

        System.out.println(result.getNow("Bye")); //semelyik CompletableFuture nem végzett.
        sleep(1500);
        System.out.println(result.getNow("Bye")); //1000ms maradt a extraLongTask-ból
        result.runAfterBothAsync(extraLongTask, () -> System.out.println("After both!"), executorService);
        result.whenCompleteAsync((s, throwable) -> System.out.println("Complete: " + s), executorService);

        executorService.shutdown(); //nincs ideje lefutni a runAfterBothAsync-nek
    }

    /**
     *
     * @param sleeptime sleep time in milliseconds
     */
    private static void sleep(int sleeptime) {
        try {
            Thread.sleep(sleeptime);
        } catch (InterruptedException e) {
            System.out.println("ERROR!");
        }
    }
}