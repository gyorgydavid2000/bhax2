import java.awt.*;
import java.awt.image.*;
import java.io.*;
import javax.imageio.*;

class art
{
    public static void main(String[] args) throws Exception
    {
        BufferedImage img = null;
        try {
        img = ImageIO.read(new File("java.jpeg"));
        }
        catch (IOException e) {}

        String s =".-;o8@";
        img=resizeImage(img,100,40);
        for (int i = 0; i< img.getHeight();i++) //sor
        {
            for (int j = 0; j< img.getWidth();j++) //oszlop
            {
                int pixel=img.getRGB(j,i);
                int avg =0;
                avg += pixel>>16 & 0xff; //red
                avg += pixel>>8 & 0xff; //green
                avg += pixel & 0xff; //blue
                avg/=3;
                System.out.print(s.charAt(avg/(254/s.length()+1)));
            }
            System.out.println();
        }
    }
    static BufferedImage resizeImage(BufferedImage originalImage, int targetWidth, int targetHeight) throws IOException
    {
	    BufferedImage resizedImage = new BufferedImage(targetWidth, targetHeight, BufferedImage.TYPE_INT_RGB);
	    Graphics2D graphics2D = resizedImage.createGraphics();
	    graphics2D.drawImage(originalImage, 0, 0, targetWidth, targetHeight, null);
	    graphics2D.dispose();
	    return resizedImage;
	}
}
