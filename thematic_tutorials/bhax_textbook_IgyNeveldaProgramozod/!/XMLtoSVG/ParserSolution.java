import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.TransformerException;
import java.io.FileWriter;
import java.io.IOException;

public class ParserSolution {

    private static final String INPUT_FILE = "input.xml";
    private static final String OUTPUT_FILE = "map.svg";

    public static void main(String[] args) throws Exception
    {
        ParserSolution ps = new ParserSolution();
        ps.parse();
    }

    public void parse() throws XMLStreamException, TransformerException, FileNotFoundException, IOException {
        List<City> cityList = createCityList();
        Map<String, String> colorsOfStates = createColorsOfStates(cityList);
        createSVG(cityList, colorsOfStates);
    }

    private void createSVG(List<City> cityList, Map<String, String> colorsOfStates) throws IOException
    {
        FileWriter fw = new FileWriter(OUTPUT_FILE);
        fw.write("<svg width=\"800\" height=\"800\">");
        for (int i = 0; i< cityList.size(); i++)
        {
            String s ="<circle cx=\"" + cityList.get(i).coordinateY.toString() +  "\" cy=\"" + cityList.get(i).coordinateX.toString() +  "\" fill=\"" + colorsOfStates.get(cityList.get(i).getState()).toString() +  "\" r=\"1\"/>";
            fw.write(s);
        }
        fw.write("</svg>");
        fw.close();
    }
    
    private Map<String, String> createColorsOfStates(List<City> cityList) {
        return cityList.stream()
                .map(City::getState)
                .distinct()
                .collect(Collectors.toMap(hexColor -> hexColor, hexColor -> randomHexColor()));
    }

    private List<City> createCityList() throws XMLStreamException, FileNotFoundException {
        XMLStreamReader xmlStreamReader = XMLInputFactory.newFactory().createXMLStreamReader(new FileInputStream(INPUT_FILE));
        List<City> cityList = new LinkedList<>();
        City c;
        while (xmlStreamReader.hasNext()) {
            int actual = xmlStreamReader.next();
            if (isStartElement(actual, "coordinateX", xmlStreamReader)) {
                c = cityList.get(cityList.size()-1);
                c.coordinateX=xmlStreamReader.getElementText();
                cityList.set(cityList.size()-1, c);
            } else if (isStartElement(actual, "coordinateY", xmlStreamReader)) {
                c = cityList.get(cityList.size()-1);
                c.coordinateY=xmlStreamReader.getElementText();
                cityList.set(cityList.size()-1, c);
            } else if (isStartElement(actual, "state", xmlStreamReader)) {
                c = cityList.get(cityList.size()-1);
                c.state=xmlStreamReader.getElementText();
                cityList.set(cityList.size()-1, c);
            } else if (isStartElement(actual, "city", xmlStreamReader)) {
                cityList.add(new City());
            }
        }
        return cityList;
    }

    private boolean isStartElement(int actual, String tagName, XMLStreamReader xmlStreamReader) {
        return actual == XMLStreamReader.START_ELEMENT && tagName.equals(xmlStreamReader.getLocalName());
    }

    private String randomHexColor() {
        return String.format("#%06X", new Random().nextInt(0x1000000));
    }

}
