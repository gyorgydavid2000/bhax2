import java.util.Scanner;
import java.io.FileWriter;

class cesar
{
    public static void main(String[] args) throws Exception
    {
        int eltolas=10;
        Scanner sc = new Scanner(System.in);
        FileWriter fw = new FileWriter("out.txt");
        String sor="";
        while (true)
        {
            sor=sc.nextLine();
            if (sor.isEmpty())
                break;
            fw.write(titkosit(sor,eltolas) + "\n");
        }
        sc.close();
        fw.close();
    }

    static String titkosit(String s, int eltol)
    {
        String out="";
        for (int i=0; i<s.length(); i++)
            out+=(char)(s.charAt(i)+eltol);
        return out;
    }
}