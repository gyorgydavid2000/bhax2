public class liskovrafigyel{
// ez a T az LSP-ben
static class Jarmu {
// public
//  void gurul(){};
};

// ez a két osztály alkotja a "P programot" az LPS-ben
static class Program {
public
     void fgv ( Jarmu jarmu ) {
          // jarmu.gurul(); a jarmu már nem tud gurulni
          // s hiába lesz a leszármazott típusoknak
          // gurul metódusa, azt a Jarmu jarmu-ra úgysem lehet hívni
     }
};

// itt jönnek az LSP-s S osztályok
static class GuruloJarmu extends Jarmu {
public
      static void gurul() {};
};

static class Kocsi extends GuruloJarmu
{};

static class Hajo extends Jarmu
{};

public static void main(String[] args){
    Program program = new Program();
    Jarmu  jarmu=new Jarmu();
    program.fgv ( jarmu );

    Kocsi kocsi=new Kocsi();
    program.fgv ( kocsi );

    Hajo hajo= new Hajo();
    program.fgv ( hajo ); // sérül az LSP, mert a P::fgv gurultatná a Hajót de a Hajó nem tud gurulni.

}
}

