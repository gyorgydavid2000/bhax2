#include <iostream>

class Szulo{
public:
    void Beszel(){
        std::cout<<"Az ős"<<std::endl;
    }
};

class Gyerek: public Szulo{
public:
    void Beszel1(){
        std::cout<<"A gyerek"<<std::endl;
    }
};

int main(){
    
    Szulo* p= new Szulo();
    Szulo* p1= new Gyerek();
    
    p->Beszel();
    p1->Beszel();
    
    //p1->Beszel1(); Ez nem működik mivel csak az ős metódusait őrökölte
    
    delete p;
    delete p1;

    return 0;
}
