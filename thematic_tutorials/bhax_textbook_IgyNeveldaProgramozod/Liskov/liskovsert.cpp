// ez a T az LSP-ben
class Jarmu {
public:
     virtual void gurul() {};
};

// ez a két osztály alkotja a "P programot" az LPS-ben
class Program {
public:
     void fgv ( Jarmu &jarmu ) {
          jarmu.gurul();
     }
};

// itt jönnek az LSP-s S osztályok
class Kocsi : public Jarmu
{};

class Hajo: public Jarmu // ezt úgy is lehet/kell olvasni, hogy a hajo tud gurulni
{};

int main ( int argc, char **argv )
{
     Program program;
     Jarmu jarmu;
     program.fgv ( jarmu );

     Kocsi kocsi;
     program.fgv ( kocsi );

     Hajo hajo;
     program.fgv ( hajo ); // sérül az LSP, mert a P::fgv gurítaná a Hajót, ami ugye lehetetlen.

}


