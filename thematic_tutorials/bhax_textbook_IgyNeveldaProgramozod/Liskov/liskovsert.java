public class liskovsert{

static class Jarmu{
    public
        void gurul(){};
}
static class Program{
       static void fgv(Jarmu jarmu){
            jarmu.gurul();
        }
}
// itt jönnek az LSP-s S osztályok
static class Kocsi extends Jarmu{}
static class Hajo extends Jarmu{} // ezt úgy is lehet/kell olvasni, hogy a hajó tud gurulni

public static void main(String[] args) {
    Program program = new Program();
    Jarmu  jarmu=new Jarmu();
    program.fgv ( jarmu );

    Kocsi kocsi=new Kocsi();
    program.fgv ( kocsi );

    Hajo hajo= new Hajo();
    program.fgv ( hajo ); // sérül az LSP, mert a P::fgv gurultatná a Hajót de a Hajó nem tud gurulni.

}
}
