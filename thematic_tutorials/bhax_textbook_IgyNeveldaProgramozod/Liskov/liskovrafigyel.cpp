// ez a T az LSP-ben
class Jarmu {

};

// ez a két osztály alkotja a "P programot" az LPS-ben
class Program {
public:
     void fgv ( Jarmu &jarmu ) {
          // jarmu.gurul(); a jármű már nem tud gurulni
          // s hiába lesz a leszármazott típusoknak
          // gurul metódusa, azt a Jarmu& jarmu-ra úgysem lehet hívni
     }
};

// itt jönnek az LSP-s S osztályok
class GuruloJarmu : public Jarmu {
public:
     virtual void gurul() {};
};

class Kocsi : public GuruloJarmu
{};

class Hajo : public Jarmu
{};

int main ( int argc, char **argv )
{
     Program program;
     Jarmu jarmu;
     program.fgv ( jarmu );

     Kocsi kocsi;
     program.fgv ( kocsi );

     Hajo hajo;
     program.fgv ( hajo );

}


