import java.util.Scanner;

public class Program
{
	public static void main(String[] args)
	{
	System.out.println("Class (Fénymásoló)\n");
	Fénymásoló f1 = new Fénymásoló();
	f1.másol(2);

	System.out.println("\nAbstract class (Kisállaok)\n");
	Kisállat Morzsi = new Kutya(15);
	Kisállat Cirmi = new Macska(4);
	Morzsi.hangot_ad();
	Morzsi.megmér();
	Cirmi.hangot_ad();
	Cirmi.megmér();
	
	System.out.println("\nInterface (Alakzat)\n");
	Négyzet n1 = new Négyzet(1);
	Kör k1 = new Kör(0.5);
	n1.rajzol();
	System.out.println(" területe: " + n1.terület());
	k1.rajzol();
	System.out.println(" területe: " + k1.terület());
	}
}

//Class
/*
Osztály: tartalmazhat függvényeket, változókat
Osztány kitrjesztése:
-Funkciók bővitése: Plusz függvényeket, változókat írhatunk hozzá.
-Funkciók módosítása: @Override segítségével felülírhatunk függvényeket.
Használat: Ha van egy (vagy több) objektum, azoknak a tulajdonságait örökölheti egy másik objektum és akár módosíthat is rajta. 
*/

class Nyomtató
{
	void nyomtat(String s)
		{
		System.out.println("[" + s + "]");
		}
}

class Fénymásoló extends Nyomtató
{
	String beolvas()
		{
		System.out.print("Ide írjon: ");
		Scanner sc = new Scanner(System.in);		
		return sc.nextLine();
		}
	void másol(int mennyiség)
		{
		String input = beolvas();
		for(int i=0; i < mennyiség; i++)
			nyomtat(input);
		}
}
//@Override

//Abstract class
/*
Nem lehet példányosítani, (mert vannak benne üres függvények).
Absztrakt osztály: tartalmaznia kell legalább egy absztrakt metóust.
Absztrakt metódus: törzse üres, az alosztályban kell deklarálni.
Használat: ha van több olyan hasonló objektum amelyeknek vannak hasoló függvényei, de mindegyiknél más az implementáció.
*/

abstract class Kisállat
{
	protected int suly;
	void megmér()
		{
		System.out.println("Kisállat súlya: " + suly +" kg");
		}
	abstract public void hangot_ad();
}

class Kutya extends Kisállat
{
	public void hangot_ad()
		{
		System.out.println("Vau!");
		}
	Kutya(int suly)
		{
		super.suly=suly;
		}
}

class Macska extends Kisállat
{
	public void hangot_ad()
		{
		System.out.println("Mijáú!");
		}
	Macska(int suly)
		{
		super.suly=suly;
		}
}

//Interface
/*
Az interfacebe csak abstract függvények találhatóak. Ezért elhagyható az abstract kulcsszó.
Akkor használjuk ha nincsennek közös tulajdonségai az objektumoknak de vannak egyéni implementációi az egyes függvényeknek.
Csak tervezési célokat szolgál.
*/

interface Alakzat
{
double terület();
void rajzol();
}

class Négyzet implements Alakzat
{	
	double a;
	Négyzet(double a)
		{
		this.a=a;
		}
	public double terület()
		{
		return a*a;
		}
	public void rajzol()
		{
		System.out.print("□");
		}
}

class Kör implements Alakzat
{
	double r;
	Kör(double r)
		{
		this.r=r;
		}
	public double terület()
		{
		return r*r*Math.PI;
		}
	public void rajzol()
		{
		System.out.print("○");
		}
}
