import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

class CountChars
{
    public static void main(String[] args) throws FileNotFoundException
    {
        // karakterek beolvasása és hozzáadása a gyakorisági listához.
        File f = new File("WW2_wikipedia.txt");
        Scanner sc = new Scanner(f);
        List<CharCount> l = new ArrayList<CharCount>();
        while(sc.hasNext())
        {
            String sor = sc.nextLine();
            for (int i=0;i<sor.length();i++)
            {
                add(l,sor.charAt(i));
            }
        }

        // lista renezése
        Collections.sort(l);

        // karakterek és előfordulásuk száma, illetve gyakoriság szint csökkenő sorrendbe kiiratása.
        String s ="";
        for (int i=0;i<l.size();i++)
        {
            System.out.println(l.get(i));
            s+=l.get(i).ch;
        }
        System.out.println("[" + s + "]");
        sc.close();
    }

    // saját osztály ami tárol egy karaktert és hogy hányszor fordul elő
    static class CharCount implements Comparable<CharCount>
    {
        char ch;
        int count;

        CharCount(char ch)
        {
            this.ch=ch;
            count=1;
        }

        @Override // System.out.println megivásakor ezt a toSring-et használja, nem a Object.class-ban lévőt.
        public String toString()
        {
            return ch + " " + count;
        }
        @Override // Comparable intrface implementálása, hogy a Collections.sort-al redezhessük.
        public int compareTo(CharCount o)
        {
            return o.count-this.count;
        }
    }

    // Ha van már a listában adott karakter, akkor megnöveli a számát. Ha nincs, akkor hozzáadja a listához.
    static void add(List<CharCount> l, char c)
    {
        int index=-1;
        for(int i=0;i<l.size();i++)
        {
            if ((char)l.get(i).ch==(char)c)
            {
                index=i;
                break;
            }
        }
        if (index==-1)
        {
            CharCount cc = new CharCount(c);
            l.add(cc);
        }
        else
        {
            CharCount cc = l.get(index);
            cc.count++;
            l.set(index, cc);
        }
    }
}