import java.io.File;
import java.util.Scanner;
import java.io.FileWriter;

public class encode {
    public static void main(String[] args) throws Exception
    {
        // kulcspár generálás, a titkos kulcsot nem mentjük el.
        KulcsPar jSzereplo = new KulcsPar();

        // bemeneti és kimeneti file.
        File f = new File("WW2_wikipedia.txt");
        Scanner sc = new Scanner(f);
        FileWriter fw = new FileWriter("WW2_titkos.txt");

        // soronkét olvas és karakternként titkosít.
        // a titkosított fileba soronként van egy BigInteger.
        // az üres sor új sort jelez.
        while (sc.hasNextLine())
        {
            String tisztaSzoveg = sc.nextLine();
            byte[] buffer = tisztaSzoveg.getBytes();
            java.math.BigInteger[] titkos = new java.math.BigInteger[buffer.length];
            for (int i = 0; i < titkos.length; ++i) {
                titkos[i] = new java.math.BigInteger(new byte[] {buffer[i]});
                titkos[i] = titkos[i].modPow(jSzereplo.e, jSzereplo.m);
                fw.write(titkos[i].toString()+"\n");
            }
            fw.write("\n");
        }
        sc.close();
        fw.close();
        System.out.println("Done");
    }
}