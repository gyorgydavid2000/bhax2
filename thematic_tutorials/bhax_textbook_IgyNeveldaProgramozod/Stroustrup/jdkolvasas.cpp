#include <iostream>
#include <boost/filesystem.hpp>
using namespace std;
using namespace boost::filesystem;

void read_acts ( boost::filesystem::path path)
{

        if ( is_regular_file ( path ) ) {

                std::string ext ( ".java" );
                if ( !ext.compare ( boost::filesystem::extension ( path ) ) ) {

                    cout<<path.filename()<<endl;

                }

        } else if ( is_directory ( path ) )
                for ( boost::filesystem::directory_entry & entry : boost::filesystem::directory_iterator ( path ) )
                        read_acts ( entry.path());

}


int main(int argc, char* argv[])
{
  if (argc < 2)
  {
    cout << "Usage: jdkolvasas path\n";
    return 1;
  }
  
  boost::filesystem::path p(argv[1]);
  
  read_acts(p);
  
  return 0;
}
  
