import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.math.BigInteger;

class decode
{
    public static void main(String[] args) throws FileNotFoundException
    {
        //BigIntegerek beolvasáasa, üres sor: -1
        final BigInteger ujsor = BigInteger.valueOf(-1);
        File f = new File("WW2_titkos.txt");
        Scanner sc = new Scanner(f);
        List<BigInteger> l = new ArrayList<BigInteger>();
        while(sc.hasNext())
        {
            String sor = sc.nextLine();
            if  (sor.isEmpty())
                l.add(ujsor);
            else
                l.add(new BigInteger(sor));
        }

        //BigIntegerek és előfordulásuk száma
        List<BigIntCount> l2 = new ArrayList<BigIntCount>();
        for (int i=0;i<l.size();i++)
        {
            if (l.get(i).compareTo(ujsor) != 0)
                add(l2,l.get(i));
        }

        //BigIntegerek gyakoriság szerint csökkenő sorrendben
        Collections.sort(l2);

        //Karakterek gyakoriság szerint csökkenő sorrendben
        String char_gyak =" etaniroshldcumfpgwy,bv.1Ak0TGB9SIF-RCWHP2MOE\"87x5L4Nj()63D'UzJKVq;/:–%YZ—XQć$üé!Čè£_šáó[]‘žěäčòâç&ß?=";
        
        // gyakoriság alapú törés
        for (int i=0;i<l.size();i++)
        {
            if (l.get(i).compareTo(ujsor) == 0)
            {
                System.out.println();
                continue;
            }

            int index=0;
            for (int j=0;j<l2.size();j++)
            {
                if (l.get(i).compareTo(l2.get(j).bi) == 0)
                {
                    index=j;
                    break;
                }
            }
            
            if (index<char_gyak.length())
                System.out.print(char_gyak.charAt(index));
            else
                System.out.print("?");
        }
        sc.close();
    }

    // saját osztály ami tárol egy BigIntegert és hogy hányszor fordul elő
    static class BigIntCount implements Comparable<BigIntCount>
    {
        BigInteger bi;
        int count;

        BigIntCount(BigInteger bi)
        {
            this.bi=bi;
            count=1;
        }

        @Override
        public String toString() 
        {
            return bi + " " + count;
        }
        @Override
        public int compareTo(BigIntCount o)
        {
            return o.count-this.count;
        }
    }

    // Ha van már a listában adott BigInteger, akkor megnöveli a számát. Ha nincs, akkor hozzáadja a listához.
    static void add(List<BigIntCount> l, BigInteger bi)
    {
        int index=-1;
        for(int i=0;i<l.size();i++)
        {   
            if (l.get(i).bi.compareTo(bi)==0)
            {
                index=i;
                break;
            }
        }
        if ((int)index==(int)-1)
        {
            BigIntCount bic = new BigIntCount(bi);
            l.add(bic);
        }
        else
        {
            BigIntCount bic = l.get(index);
            bic.count++;
            l.set(index, bic);
        }
    }
}