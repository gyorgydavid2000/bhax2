library(matlab)

f <- function(x){
	primek=primes(x)
	diff=primek[2:length(primek)]-primek[1:length(primek)-1]
	index=which(diff==2)
	osszeg=1/primek[index]+1/(primek[index]+2)
	return(sum(osszeg))
	}

x=seq(13, 1000000, by=10000)
y=sapply(x, FUN = f)
plot(x,y,type="b")

